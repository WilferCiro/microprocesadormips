#!/usr/bin/env python


# Custom libraries
from src.editor import HighlighterASM

# Qt libraries
from PyQt5.QtWidgets import QWidget, QApplication
from PyQt5 import uic
from PyQt5.QtGui import QFont


# System libraries
import os

class MainWidget(QWidget):

	def __init__(self):
		super(QWidget, self).__init__()
		#self.setupUi(self)
		uic.loadUi("src/ui/mainWidget.ui", self)
		
		# Fields format
		font = QFont()
		font.setFamily('Courier')
		font.setFixedPitch(True)
		font.setPointSize(11)
		self.codeText.setFont(font)
		self.codeTextASM = HighlighterASM(self.codeText.document())
		
