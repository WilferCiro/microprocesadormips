#!/usr/bin/env python


# Custom libraries

# Qt libraries
from PyQt5.QtCore import QFile, QRegExp, Qt
from PyQt5.QtGui import QFont, QSyntaxHighlighter, QTextCharFormat, QColor
from PyQt5.QtWidgets import QTextEdit

# System libraries

class Highlighter(QSyntaxHighlighter):
	def __init__(self, parent=None):
		super(Highlighter, self).__init__(parent)
		self.getCharFormat()

	def getCharFormat(self):
		pass

	def highlightBlock(self, text):
		for pattern, format in self.highlightingRules:
			expression = QRegExp(pattern)
			index = expression.indexIn(text)
			while index >= 0:
				length = expression.matchedLength()
				self.setFormat(index, length, format)
				index = expression.indexIn(text, index + length)

		self.setCurrentBlockState(0)

		startIndex = 0
		if self.previousBlockState() != 1:
			startIndex = self.commentStartExpression.indexIn(text)

		while startIndex >= 0:
			endIndex = self.commentEndExpression.indexIn(text, startIndex)

			if endIndex == -1:
				self.setCurrentBlockState(1)
				commentLength = len(text) - startIndex
			else:
				commentLength = endIndex - startIndex + self.commentEndExpression.matchedLength()

			self.setFormat(startIndex, commentLength, self.multiLineCommentFormat)
			startIndex = self.commentStartExpression.indexIn(text, startIndex + commentLength)


class HighlighterASM(Highlighter):
	def __init__(self, parent=None):
		super(HighlighterASM, self).__init__(parent)

	def getCharFormat(self):
		keywordFormat = QTextCharFormat()
		keywordFormat.setForeground(QColor("#C778D8"))
		keywordFormat.setFontWeight(QFont.Bold)

		keywordPatterns = ["\\badd\\b", "\\band\\b", "\\bnop\\b",
			"\\bjmp\\b", "\\bbeq\\b", "\\blui\\b", "\\bsw\\b",
			"\\bfloat\\b", "\\bchar\\b", "\\bdef\\b", "\\bimport\\b", "\\bclass\\b"]

		self.highlightingRules = [(QRegExp(pattern), keywordFormat) for pattern in keywordPatterns]

		classFormat = QTextCharFormat()
		classFormat.setFontWeight(QFont.Bold)
		classFormat.setForeground(Qt.darkMagenta)
		self.highlightingRules.append((QRegExp("\\bQ[A-Za-z]+\\b"), classFormat))

		singleLineCommentFormat = QTextCharFormat()
		singleLineCommentFormat.setForeground(QColor("#53615E"))
		self.highlightingRules.append((QRegExp("#[^\n]*"), singleLineCommentFormat))

		numberFormat = QTextCharFormat()
		numberFormat.setForeground(QColor("#53615E"))
		self.highlightingRules.append((QRegExp("[0-9]"), numberFormat))

		self.multiLineCommentFormat = QTextCharFormat()
		self.multiLineCommentFormat.setForeground(QColor("#53615E"))

		quotationFormat = QTextCharFormat()
		quotationFormat.setForeground(QColor("#DBAC60"))
		self.highlightingRules.append((QRegExp("\".*\""), quotationFormat))

		functionFormat = QTextCharFormat()
		functionFormat.setFontItalic(True)
		functionFormat.setForeground(QColor("#DBAC60"))
		self.highlightingRules.append((QRegExp("\\b[A-Za-z0-9_]+(?=\\()"), functionFormat))

		self.commentStartExpression = QRegExp("'''")
		self.commentEndExpression = QRegExp("'''")


