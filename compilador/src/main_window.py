#!/usr/bin/env python

# Custom libraries

# Qt libraries
from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5.QtGui import QPainter
from PyQt5.QtCore import Qt

# System libraries

class MainWindow(QMainWindow):
	"""
		Principal Window
	"""
	def __init__(self):
		super(QMainWindow, self).__init__()

		# Call a instance of the application
		App = QApplication.instance()

		# widget properties
		self.setWindowTitle(self.tr("Compilador DDA"))
		self.setCentralWidget(App.mainWidget)



