#!/usr/bin/env python

# Custom libraries
from src.main_window import MainWindow
from src.main_widget import MainWidget

# Qt libraries
from PyQt5.QtWidgets import QApplication, QSystemTrayIcon
from PyQt5.QtCore import QTranslator, QRectF
from PyQt5.QtGui import QIcon

# System libraries
import sys
import os

class App(QApplication):
	"""
		Main class, defines the application class
	"""
	def __init__(self):
		super(QApplication, self).__init__(sys.argv)

		self.setWindowIcon(QIcon('src/images/icon.svg'))
		
		self.mainWidget = MainWidget()

		# Application window
		self.mainWindow = MainWindow()
		self.mainWindow.setGeometry(100, 100, 1200, 1000)
		self.mainWindow.showMaximized()

		f = self.font()
		f.setFamily("Monaco")
		f.setPointSize(9)
		self.setFont(f)


if __name__ == '__main__':
	"""
		Start application
	"""
	application = App()
	sys.exit(application.exec_())

