`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/31/2019 02:51:53 PM
// Design Name: 
// Module Name: tb_main
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_main();

    parameter BUS_SIZE = 8;
    
    reg clk;
    reg rst;
    reg [BUS_SIZE - 1 : 0] a1, a2, a3, a4;
    reg [BUS_SIZE - 1 : 0] x1, x2, x3, x4;
    wire [BUS_SIZE -1 : 0] y;
    
    main UUT(
        .clk(clk),
        .rst(rst),
        .a1(a1),
        .a2(a2),
        .a3(a3),
        .a4(a4),
        .x1(x1),
        .x2(x2),
        .x3(x3),
        .x4(x4),
        .y(y)
    );
    
    initial begin
        clk = 1'b1;
        rst = 1'b1;
        x1 = 4;
        x2 = 3;
        x3 = 2;
        x4 = 1;
        a1 = 4;
        a2 = 3;
        a3 = 2;
        a4 = 1;
        #1
        rst = 1'b0;
        #3
        rst = 1'b1;
        #3
        a1 = 4;
        a2 = 3;
        a3 = 2;
        a4 = 1;
        #5;
        
    end
    
    always begin
        #1 clk = ~clk;
    end
    
endmodule
