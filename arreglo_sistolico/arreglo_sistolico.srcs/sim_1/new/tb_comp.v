`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/31/2019 02:23:06 PM
// Design Name: 
// Module Name: tb_comp
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_comp();

    parameter BUS_SIZE = 8;
    
    reg clk;
    reg rst;
    reg [BUS_SIZE - 1 : 0] a;
    reg [BUS_SIZE - 1 : 0] x;
    reg [BUS_SIZE -1 : 0] x_in;
    wire [BUS_SIZE -1 : 0] y;
    
    comp UUT(
        .clk(clk),
        .rst(rst),
        .a(a),
        .x(x),
        .x_in(x_in),
        .y(y)
    );
    
    initial begin
        clk = 1'b1;
        rst = 1'b1;
        #1
        rst = 1'b0;
        #3
        rst = 1'b1;
        #1
        a = 8'd10;
        x_in = 8'd8;
        x = 8'b0;
        #5
        a = 8'd20;
        #5;
    end
    
    always begin
        #1 clk = ~clk;
    end
    
    

endmodule
