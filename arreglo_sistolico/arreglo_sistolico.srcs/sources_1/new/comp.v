`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Uniquindío
// Engineer: Wilfer Ciro y Mateo Sánchez
// 
// Create Date: 10/31/2019 02:09:40 PM
// Design Name: 
// Module Name: comp
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module comp(clk, rst, a, x, x_in, y);
    parameter BUS_SIZE = 8;
    
    input clk;
    input rst;
    input [BUS_SIZE - 1 : 0] a;
    input [BUS_SIZE - 1 : 0] x;
    input [BUS_SIZE -1 : 0] x_in;
    output reg [BUS_SIZE -1 : 0] y;
    
    reg [2:0]COUNTER = 0;
    reg [BUS_SIZE * 2 - 1 : 0]SUMA = 0;
    
    always @(posedge clk) begin
        if (!rst)
            COUNTER = 3'b0;
        else begin
            if (COUNTER < 3'd3)
                COUNTER = COUNTER + 3'b001;
            else
                COUNTER = 0;
        end            
    end
    
    always @(posedge clk) begin
        if (!rst) begin
            y = x_in;
            SUMA = 0;
        end else begin
            if (COUNTER < 3'd3) begin
                y = x;
                SUMA  = SUMA + (x * a); 
            end else begin
                y = SUMA + (x * a);
                SUMA = 0;
            end
        end
    end
endmodule
