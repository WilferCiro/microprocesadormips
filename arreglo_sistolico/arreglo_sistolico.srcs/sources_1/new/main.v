`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/31/2019 02:41:35 PM
// Design Name: 
// Module Name: main
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module main(a1, a2, a3, a4, x1, x2, x3, x4, clk, rst, y);
    parameter BUS_SIZE = 8;
    
    input clk;
    input rst;
    input [BUS_SIZE - 1 : 0] a1, a2, a3, a4;
    input [BUS_SIZE -1 : 0] x1, x2, x3, x4;
    output [BUS_SIZE -1 : 0] y;
    
    wire [BUS_SIZE -1 : 0] y1, y2, y3, y4;
    
    assign y = y4;
    
    comp #(.BUS_SIZE(BUS_SIZE)) part1(
        .clk(clk),
        .rst(rst),
        .a(a1),
        .x(y4),
        .x_in(x1),
        .y(y1)
    );
    
    comp #(.BUS_SIZE(BUS_SIZE)) part2(
        .clk(clk),
        .rst(rst),
        .a(a2),
        .x(y1),
        .x_in(x2),
        .y(y2)
    ); 
    
    comp #(.BUS_SIZE(BUS_SIZE)) part3(
        .clk(clk),
        .rst(rst),
        .a(a3),
        .x(y2),
        .x_in(x3),
        .y(y3)
    ); 
    
    comp #(.BUS_SIZE(BUS_SIZE)) part4(
        .clk(clk),
        .rst(rst),
        .a(a4),
        .x(y3),
        .x_in(x4),
        .y(y4)
    );   
    
    
endmodule
