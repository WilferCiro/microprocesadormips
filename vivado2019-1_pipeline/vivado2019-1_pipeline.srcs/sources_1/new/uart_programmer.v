`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/09/2019 10:22:06 AM
// Design Name: 
// Module Name: uart_programmer
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module uart_programmer(clk, rst, rx, valOut);
    input clk;
    input rst;
    input rx;
    output [7:0] valOut;
    
    reg CS, NS;
    
    parameter IDLE = 0;
    parameter WAITING_INITIAL = 1;
    parameter WAITING = 2;
    parameter READING = 3;
    reg nro_readed = 0;
    reg countNumber = 0;
    reg limit = 0;
    reg pased_limit = 0;
    
    always @(posedge clk) begin
        if (rst)
            CS = IDLE;
        else
            CS = NS;
    end
    
    always @(CS) begin
        case (CS)
            IDLE: begin
                if (rx == 1'b0)
                    NS = WAITING_INITIAL;
                else
                    NS = IDLE;
            end
            WAITING_INITIAL: begin
                limit = 5042;
                if (pased_limit == 1'b1)
                    NS = WAITING;
                else
                    NS = WAITING_INITIAL;
            end
            
        endcase
    end
    
    
endmodule
