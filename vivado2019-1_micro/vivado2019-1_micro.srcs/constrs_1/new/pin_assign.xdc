set_property PACKAGE_PIN L16 [get_ports clk]
set_property PACKAGE_PIN Y16 [get_ports rst]
set_property PACKAGE_PIN M15 [get_ports flagOut]

set_property IOSTANDARD LVCMOS12 [get_ports clk]
set_property IOSTANDARD LVCMOS12 [get_ports flagOut]
set_property IOSTANDARD LVCMOS12 [get_ports rst]
