/*
	2019-2
*/

module data_path(opCode, MemaReg, enWrSram, ALUSelector, enablePC, fteALU, clk, rst, regDst, flagZ, dirSelPC, ALU_res_memory, outMemory_memory, DRB_reg_file_memory);

	// Parameters declarations
	parameter BUS_SIZE = 32;
	parameter DIR_SIZE = BUS_SIZE;
	parameter OPC_SIZE = BUS_SIZE; 
	parameter DIR_SIZE_INTERNAL = 5; // No change
	
	// Control wires
	input clk;
	input rst;
	input MemaReg;
	input enWrSram;
	input [2 : 0] ALUSelector;
	input enablePC;
	input fteALU;
	input regDst;
	
	output flagZ;
	input [1:0] dirSelPC;
	
	
	output [BUS_SIZE - 1 : 0] ALU_res_memory;
	input [BUS_SIZE - 1 : 0] outMemory_memory;
	output [BUS_SIZE - 1 : 0] DRB_reg_file_memory;	
	
	
	// Internal Wires	
	output [OPC_SIZE - 1 : 0] opCode;
	wire [BUS_SIZE - 1 : 0] DRA_reg_file;
	wire [BUS_SIZE - 1 : 0] DRB_reg_file;	
	wire [BUS_SIZE - 1 : 0] ALU_res;
	wire [BUS_SIZE - 1 : 0] DW_reg_file;
	wire [BUS_SIZE - 1 : 0] outMemory;
	wire [BUS_SIZE - 1 : 0] ALU_inB;
	wire [BUS_SIZE - 1 : 0] extendedData16;
	wire [BUS_SIZE - 1 : 0] extendedData6;
	wire [DIR_SIZE - 1 : 0] dirPC;
	wire [DIR_SIZE_INTERNAL - 1 : 0] dirWriteRF;
	
	// Internal wires
	wire flagN, flagC;
	
	// Connections and asignations
	assign DW_reg_file = MemaReg ? outMemory : ALU_res;
	assign ALU_inB = fteALU ? extendedData16 : DRB_reg_file;
	assign dirWriteRF = regDst ? opCode[15 : 11] : opCode[20 : 16];
	
	assign ALU_res_memory = ALU_res; 
	assign outMemory = outMemory_memory; 
	assign DRB_reg_file_memory = DRB_reg_file; 
	
	// Connect the register file
	sram #(.BUS_SIZE(BUS_SIZE), .DIR_SIZE_INTERNAL(DIR_SIZE_INTERNAL)) RegFile (
		.outA(DRA_reg_file),
		.outB(DRB_reg_file),
		.clk(clk),
		.enWrite(enWrSram),
		.writeData(DW_reg_file),
		.dirWrite(dirWriteRF), // Rd
		.dirA(opCode[25 : 21]), // Rs
		.dirB(opCode[20 : 16]) // Rt
	);
	
	// connect the ROM memory that has the firmware
	flash #(.BUS_SIZE(BUS_SIZE), .DIR_SIZE(DIR_SIZE), .OPC_SIZE(OPC_SIZE)) MyRunner(
		.dir(dirPC),
		.opCode(opCode)
	);
	
	// Connect the ALU	
	alu #(.BUS_SIZE(BUS_SIZE)) MyALU (
		.A(DRA_reg_file),
		.B(ALU_inB),
		.selector(ALUSelector),
		.R(ALU_res),
		.flagZ(flagZ),
		.flagN(flagN),
		.flagC(flagC)
	);
	
	// Connect the program memory
	/*memory #(.BUS_SIZE(BUS_SIZE)) MyMemory (
		.pOut(outMemory),
		.clk(clk),
		.rst(rst),
		.enWrite(enWriteMemory),
		.writeData(DRB_reg_file),
		.dirIn(ALU_res)
	);*/
	
	// Connect the module for extend the data sign
	extend_sign #(.BUS_IN(16), .BUS_SIZE(BUS_SIZE)) ex_signal16(
		.dataIn(opCode[15 : 0]),
		.dataOut(extendedData16)
	);
	
	extend_sign #(.BUS_IN(26), .BUS_SIZE(BUS_SIZE)) ex_signal6(
		.dataIn(opCode[25 : 0]),
		.dataOut(extendedData6)
	);
	
	// Connect the program counter (PC)
	program_counter #(.DIR_SIZE(DIR_SIZE)) MyPC(
		.clk(clk),
		.rst(rst),
		.enable(enablePC),
		.dirInBranch(extendedData16),
		.dirInJump(extendedData6),
		.dirSel(dirSelPC),
		.dirOut(dirPC)
	);

endmodule 
