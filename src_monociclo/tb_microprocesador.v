`timescale 1ns / 1ps
/*
	2019-2
*/

module tb_microprocesor;
	reg clk;
	reg rst;
	// Instance the microprocesador module
	microprocesador UUT(
		.clk(clk),
		.rst(rst)
	);

	initial begin
		clk = 1'b0;
		rst = 1'b1;
		#2
		rst = 1'b0;
		#200;
	
	end
	
	always begin
		#1 clk = ~clk;
	end

endmodule 
