/*
	2019-2
*/
/*
	dirSel  | output
	0		| dir + 1
	1		| dirIn
*/
/*
	Memory max size: 2^DIR_SIZE = 255 (ex: 2^8 = 256)
*/

module program_counter(clk, rst, enable, dirInBranch, dirInJump, dirSel, dirOut);
	// parameters declarations
	parameter DIR_SIZE = 32;
	
	// Inputs declarations
	input clk;
	input rst;
	input enable;
	input [1 : 0] dirSel;
	input [DIR_SIZE - 1 : 0] dirInBranch;
	input [DIR_SIZE - 1 : 0] dirInJump;
	
	// Outputs declarations
	output reg [DIR_SIZE - 1 : 0] dirOut;
	
	// Logic block
	always @(posedge clk)
	begin
		if (!rst)
			dirOut <= {DIR_SIZE{1'b0}};
		else if (enable)
			if (dirSel[0] == 1'b1)
				dirOut <= dirInBranch + dirOut;
			else if (dirSel[1] == 1'b1)
				dirOut <= dirInJump + dirOut;
			else
				dirOut <= dirOut + {{(DIR_SIZE - 1){1'b0}}, 1'b1};
			
	end

endmodule 
