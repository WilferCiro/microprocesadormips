/*
	2019-2
*/

module microprocesador(clk, rst, flagOut);
	// parameters definitions
	parameter BUS_SIZE = 32;
	parameter DIR_SIZE = BUS_SIZE;
	parameter OPC_SIZE = BUS_SIZE; 
	parameter DIR_SIZE_INTERNAL = 5; // No change
	
	// Inputs
	input clk;
	input rst;
	output flagOut;
	
	// Internal wires
	wire MemaReg;
	wire rst_n;
	wire enWrSram;
	wire [2 : 0] ALUSelector;
	wire enWriteMemory;
	wire branch;
	wire jump;
	wire enablePC;
	wire fteALU;
	wire regDst;
	wire [OPC_SIZE - 1 : 0] opCode;
	
	wire flagZ;
	wire [1:0] dirSelPC;
	
	wire [BUS_SIZE - 1 : 0] outMemory;
	wire [BUS_SIZE - 1 : 0] DRB_reg_file;
	wire [BUS_SIZE - 1 : 0] ALU_res;	
	
	assign flagOut = flagZ;	
	assign rst_n = ~rst;
	
	// Connection of data_path and control_path
	
	data_path #(.BUS_SIZE(BUS_SIZE), .DIR_SIZE(DIR_SIZE)) myData(
		.opCode(opCode),
		.MemaReg(MemaReg),
		.enWrSram(enWrSram),
		.ALUSelector(ALUSelector),
		.enablePC(enablePC),
		.fteALU(fteALU),
		.clk(clk),
		.rst(rst_n),
		.regDst(regDst),
		.dirSelPC(dirSelPC),
		.flagZ(flagZ),
		.ALU_res_memory(ALU_res),
		.outMemory_memory(outMemory),
		.DRB_reg_file_memory(DRB_reg_file)
	);
	
	
	control_path #(.BUS_SIZE(BUS_SIZE), .DIR_SIZE(DIR_SIZE)) myControl(
		.opCode(opCode[31:26]),
		.functionCode(opCode[5:0]),
		.MemaReg(MemaReg),
		.enWrSram(enWrSram),
		.ALUSelector(ALUSelector),
		.enWriteMemory(enWriteMemory),
		.fteALU(fteALU),
		.enablePC(enablePC),
		.clk(clk),
		.rst(rst_n),
		.regDst(regDst),
		.dirSelPC(dirSelPC),
		.flagZ(flagZ)	
	);
	
	
	// Connect the program memory
	memory #(.BUS_SIZE(BUS_SIZE)) MyMemory (
		.pOut(outMemory),
		.clk(clk),
		.enWrite(enWriteMemory),
		.writeData(DRB_reg_file),
		.dirIn(ALU_res[7 : 0])
	);
	
	
	// Connect probes 
	
    CompILA test (
        .clk(clk),
        .probe0(opCode),
        .probe1(flagZ),
        .probe2(microprocesador.myData.RegFile.MEM[1])
    );
	

endmodule 
