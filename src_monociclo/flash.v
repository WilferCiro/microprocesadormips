/*
	2019-2
*/

module flash(dir, opCode);
	// parameters definitions
	parameter BUS_SIZE = 32;
	parameter DIR_SIZE = 32;
	parameter OPC_SIZE = BUS_SIZE;
	
	parameter MEM_SIZE = 10;
	
	// inputs declarations
	input [DIR_SIZE - 1 : 0] dir;
	
	// outputs declarations
	output [OPC_SIZE - 1 : 0] opCode;
	
	// firmware, change for the correct firmware to exec
	reg [OPC_SIZE - 1 : 0] ROM [0 : MEM_SIZE];
	initial
	begin
		//$readmemb("/home/ciro/Documents/Universidad/Semestre9/DDA/ROMMicro.mem", ROM);
		ROM[0] = 32'b001111_00000_00001_00000_00000000001; // load immediate in 1 the value of 1
		ROM[1] = 32'b001111_00000_00010_00000_00000000001; // load immediate in 2 the value of 1
		ROM[2] = 32'b001111_00000_00011_00000_00000000010; // load immediate in 3 the value of 2
		ROM[3] = 32'b000000_00001_00010_00001_00000100000; // Add dir 1 with dir 2 and store in dir 1
		//ROM[4] = 32'b000000_00000_00000_00000_00000000000; // NOP
		ROM[4] = 32'b000100_00001_00011_11111_11111111111;
		ROM[5] = 32'b000010_11111_11111_11111_11111111110;
	end
	/*
	integer i;
	initial begin
		for (i = 0; i <= MEM_SIZE; i = i + 1)
			ROM[i] = 32'b0;
		ROM[0] = 32'b101011_01010_000000000000001000000;
		ROM[1] = 32'b100011_01010_000000000000000010000;
		ROM[2] = 32'b101011_01010_000000000000000000100;
		ROM[3] = 32'b100010_01010_000000000000000000010;
		ROM[4] = 32'b101011_01010_000000000000001000000;
	end
	*/
	// Data flow	
	assign opCode = ROM[dir];
endmodule 
