/*
	2019-2
*/

module control_path(opCode, functionCode, MemaReg, enWrSram, ALUSelector, enWriteMemory, enablePC, fteALU, clk, rst, regDst, flagZ, dirSelPC);
	// parameters definitions
	parameter BUS_SIZE = 32;
	parameter DIR_SIZE = 32;
	parameter OPC_SIZE = BUS_SIZE;
	
	// Inputs definitions
	input [6 - 1 : 0] opCode;
	input [6 - 1 : 0] functionCode;
	input clk;
	input rst;
	input flagZ;
	
	
	// Outputs definitions
	output reg MemaReg;
	output reg enWrSram;
	output reg [2 : 0] ALUSelector;
	output reg enWriteMemory;
	output reg enablePC;
	output reg fteALU;
	output reg regDst;
	output [1:0] dirSelPC;
	
	
	reg branch;
	reg jump;
		
	assign dirSelPC = {jump, branch & flagZ};
	
	// FSM parameters and regs
	/*reg [1:0] CS, NS;
	parameter IDLE = 0;
	parameter LUI = 1;
	parameter ADD = 2;*/
	
	// Logic
	
	/*always @(posedge clk)
	begin
		if (!rst)
			CS <= IDLE;
		else
			CS <= NS;
	end*/
	
	always @(opCode, functionCode)
	begin
		MemaReg = 1'b0;
		enWrSram = 1'b0;
		ALUSelector = 3'b000;
		enWriteMemory = 1'b0;
		
		branch = 1'b0;
		jump = 1'b0;
		
		enablePC = 1'b1;
		fteALU = 1'b0;
		regDst = 1'b0;
		
		
		if (opCode == 6'b0) begin
			regDst = 1'b1;
			enWrSram = 1'b1;
			case (functionCode)
				6'b100000: // ADD
					ALUSelector = 3'b000;
				6'b100010: // SUB
					ALUSelector = 3'b001;
				6'b100100: // AND
					ALUSelector = 3'b010;
				6'b100101: // OR
					ALUSelector = 3'b011;
				6'b100110: // XOR
					ALUSelector = 3'b100;
				6'b000000: // XOR
					ALUSelector = 3'bxxx;
				default:
					ALUSelector = 3'bxxx;
			endcase
		end
		else begin
			case(opCode)
				6'b100011: begin // lw
					enWrSram = 1'b1;	
					fteALU = 1'b1;
					MemaReg = 1'b1;
				end
				6'b101011: begin // sw
					fteALU = 1'b1;
					enWriteMemory = 1'b1;	
					
					regDst = 1'bx;		
					MemaReg = 1'bx;
				end
				6'b000100: begin // beq					
					branch = 1'b1;
					ALUSelector = 3'b001;
					
					regDst = 1'bx;	
					MemaReg = 1'bx;
				end
				6'b000010: begin // jump
					jump = 1'b1;
					regDst = 1'bx;	
					fteALU = 1'bx;
					branch = 1'bx;	
					MemaReg = 1'bx;
					ALUSelector = 3'bxxx;					
				end
				6'b001111: begin
					regDst = 1'b0;
					fteALU = 1'b1;
					enWrSram = 1'b1;
				end
				default: begin
					enWrSram = 1'bx;
					regDst = 1'bx;
					fteALU = 1'bx;
				end
			endcase
		end
			
		/*case(CS)
			IDLE: begin
				enablePC = 1'b1;
				// R Type instructions
				if (opCode[31 : 26] == 6'b0)
					if (opCode[5 : 0] == 6'b100000)
						NS = ADD;
					else
						NS = IDLE;
				// I type instructions
				else
					if (opCode[31 : 26] == 6'b001111)
						NS = LUI;
					else
						NS = IDLE;
			end
			
			// L Type Instructions			
			ADD: begin
				regDst = 1'b1;
				enWrSram = 1'b1;
				NS = IDLE;
			end
			
			// R Type instructions
			LUI: begin
				fteALU = 1'b1;
				enWrSram = 1'b1;
				NS = IDLE;
			end
			
		endcase */
	end
	
endmodule 
