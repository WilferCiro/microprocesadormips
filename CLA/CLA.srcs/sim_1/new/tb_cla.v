`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/10/2019 10:21:03 PM
// Design Name: 
// Module Name: tb_cla
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_cla(

    );
    reg [15:0] A;
    reg [15:0] B;
    reg Cli;
    wire [15:0] S;
    wire Ghl;
    wire Phl;
    
    
    main UUT(
        .A(A),
        .B(B),
        .S(S),
        .Ghl(Ghl),
        .Phl(Phl),
        .Cli(Cli)
    );
    
    initial begin
        A = 16'd0;
        B = 16'd0;
        Cli = 1'b0;
        #3
        A = 16'd1;
        B = 16'd0;
        #3
        A = 16'd65534;
        B = 16'd20;
        #3
        A = 16'd4500;
        B = 16'd520;
        #3
        A = 16'd120;
        B = 16'd350;
        #3
        A = 16'd6000;
        B = 16'd2000;
        #3
        A = 16'd1;
        B = 16'd0;
        #3
        A = 16'd1250;
        B = 16'd250;
        #3
        A = 16'd500;
        B = 16'd5500;
        #3
        A = 16'd1;
        B = 16'd0;
        #3
        A = 16'd2000;
        B = 16'd7000;
        #3
        A = 16'd600;
        B = 16'd7;
        #3
        A = 16'd6;
        B = 16'd15;
        #3
        A = 16'd5000;
        B = 16'd1600;
        #3
        A = 16'd2050;
        B = 16'd50;
        #3;
    end
endmodule
