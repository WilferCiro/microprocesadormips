`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/10/2019 09:28:50 PM
// Design Name: 
// Module Name: block_x
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module block_x(
    input A,
    input B,
    output S,
    output g,
    output p,
    input Ci
    );
    
    assign g = A & B;
    assign p = A | B;
    assign S = (A ^ B) ^ Ci;
endmodule
