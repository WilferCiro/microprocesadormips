`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/10/2019 09:28:50 PM
// Design Name: 
// Module Name: main
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module level2(
    input [3:0] A,
    input [3:0] B,
    input Cli,
    output [3:0] S,
    output G,
    output P
);

    wire [1:0] gs, ps, cs;
    
    level1 mod1(
        .A(A[3:2]),
        .B(B[3:2]),
        .S(S[3:2]),
        .G(gs[1]),
        .P(ps[1]),
        .Cli(cs[1])
    );
    
    
    level1 mod2(
        .A(A[1:0]),
        .B(B[1:0]),
        .S(S[1:0]),
        .G(gs[0]),
        .P(ps[0]),
        .Cli(cs[0])
    );
    
    
    
    block_y y(        
        .Gh(gs[1]),
        .Ph(ps[1]),
        .Ch(cs[1]),
        .Gl(gs[0]),
        .Pl(ps[0]),
        .Cli(Cli),
        .Clo(cs[0]),
        .Ghl(G),
        .Phl(P)
    );
    
    //assign Ghl = gs[1];
    //assign Phl = ps[1];
    //assign Clo = cs[1];
    
    
endmodule
