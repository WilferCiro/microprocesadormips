`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/10/2019 09:28:50 PM
// Design Name: 
// Module Name: main
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module level3(
    input [7:0] A,
    input [7:0] B,
    input Cli,
    output [7:0] S,
    output G,
    output P
);

    wire [1:0] gs, ps, cs;
    
    level2 mod1(
        .A(A[7:4]),
        .B(B[7:4]),
        .S(S[7:4]),
        .G(gs[1]),
        .P(ps[1]),
        .Cli(cs[1])
    );
    
    
    level2 mod2(
        .A(A[3:0]),
        .B(B[3:0]),
        .S(S[3:0]),
        .G(gs[0]),
        .P(ps[0]),
        .Cli(cs[0])
    );
    
    
    
    block_y y(        
        .Gh(gs[1]),
        .Ph(ps[1]),
        .Ch(cs[1]),
        .Gl(gs[0]),
        .Pl(ps[0]),
        .Cli(Cli),
        .Clo(cs[0]),
        .Ghl(G),
        .Phl(P)
    );
    
    
endmodule
