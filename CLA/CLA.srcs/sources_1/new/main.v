`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/10/2019 09:28:50 PM
// Design Name: 
// Module Name: main
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module main(
    input [15:0] A,
    input [15:0] B,
    input Cli,
    output [15:0] S,
    output Ghl,
    output Phl
);

    wire [1:0] gs, ps, cs;
    
    level3 mod1(
        .A(A[15:8]),
        .B(B[15:8]),
        .S(S[15:8]),
        .G(gs[1]),
        .P(ps[1]),
        .Cli(cs[1])
    );
    
    
    level3 mod2(
        .A(A[7:0]),
        .B(B[7:0]),
        .S(S[7:0]),
        .G(gs[0]),
        .P(ps[0]),
        .Cli(cs[0])
    );
    
    
    
    block_y y(        
        .Gh(gs[1]),
        .Ph(ps[1]),
        .Ch(cs[1]),
        .Gl(gs[0]),
        .Pl(ps[0]),
        .Cli(Cli),
        .Clo(cs[0]),
        .Ghl(Ghl),
        .Phl(Phl)
    );
    
    
endmodule
