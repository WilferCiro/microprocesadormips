`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/10/2019 09:28:50 PM
// Design Name: 
// Module Name: block_y
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module block_y(
    input Gh,
    input Ph,
    output Ch,
    input Gl,
    input Pl,
    input Cli,
    output Ghl,
    output Phl,
    output Clo
    );
    
    assign Clo = Cli;
    assign Phl = Ph & Pl;
    assign Ghl = (Ph & Gl) | Gh;
    assign Ch = (Cli & Pl) | Gl;
endmodule
