`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/11/2019 02:43:30 PM
// Design Name: 
// Module Name: level1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module level1(
    input [1:0] A,
    input [1:0] B,
    output [1:0] S,
    output G,
    output P,
    input Cli
    );
    
    wire [1:0] gs, ps, cs;

    
    block_x x1(
        .A(A[1]),
        .B(B[1]),
        .S(S[1]),
        .g(gs[1]),
        .p(ps[1]),
        .Ci(cs[1])
    );
    
    block_x x0(
        .A(A[0]),
        .B(B[0]),
        .S(S[0]),
        .g(gs[0]),
        .p(ps[0]),
        .Ci(cs[0])
    );
    
    block_y y0(        
        .Gh(gs[1]),
        .Ph(ps[1]),
        .Ch(cs[1]),
        
        .Gl(gs[0]),
        .Pl(ps[0]),
        .Clo(cs[0]),
        
        .Cli(Cli),
        .Ghl(G),
        .Phl(P)
    );
    
endmodule
